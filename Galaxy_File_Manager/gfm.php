<?php
/*
Galaxy File Manager, a web-based file manager
Copyright (C) 2006-2007, 2008 Joshua Townsend
Copyright (C) 2018-2020 Thomas Ingles

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ionos & more... NO
ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
session_start();// For PluXml (in original 1.0, line 882)

if(!isset($_SESSION['gfmuser']) OR !isset($_SESSION['gfmpass'])) exit;// 4 PluXml protect #if use standalone comment this line

// Configuration Starts Here
// You must set up at least one user and set DIR to the proper directory for proper functionality of Galaxy File Manager
$aConfig = array(
    // Configuration -> Users
    "USERS" => array(
        $_SESSION['gfmuser'] => array( // pluxml user admin login #4 standalone change user
            "password" => $_SESSION['gfmpass'], // pluxml user admin password #4 standalone change password
            ($_SESSION['profil']<1)?"admin":"dir" => ($_SESSION['profil']<1)?true:$_SESSION['medias'], // used to specify that this user is an admin or not
        ),
/*
        // Further examples:
        "johndoe" => array(
            "password" => "janedoe",
        ),
        "user" => array(
            "password" => "example",
            "dir" => "mydir", // this user would not have access to the directory specified by DIR below, but, rather, would only have access to the directory mydir within the directory specified by DIR
        ),
*/
    ),
    // Configuration -> General
    "DIR" => str_replace('core/admin','',$_SESSION['domain']),// Default PluXml directory. (#standalone default ".")   No access is permitted above this point.  If DIR contains "..", then the web browser links may not work.
    "DATE_FORMAT" => "Y/m/d&\\nb\sp;H:i:s", // The date format that is used for the Last Modified column: &\\nb\sp; = &nbsp;
    "NO_SYMLINK" => false, // If false, GFM list symlinked files and folders,
    "MAX_UPLOAD" => 0, // The maximium size (in bytes) that an uploaded file may be.  A value of 0 or lower will cause the value in php.ini to be the only value obeyed.
    "OVERRIDE_MAX_UPLOAD" => false, // If true, GFM will attempt to override the php.ini values post_max_size and upload_max_filesize with the value specified by GFM_MAX_UPLOAD
    "DEFAULT_PERMS" => 0755, // The default file permissions for uploaded and created files.  This must be an octal value, or it will not work as expected.
    // Configurations -> Blacklists
    "EXT_BLACKLIST" => "php,pl,htaccess", // Comma-separated list of disallowed extensions
    "FILE_BLACKLIST" => "php.ini", // Comma-separated list of disallowed filenames
    "IS_BLACKLISTED" => true, // If true, this file for Galaxy File Manager will be added to the blacklist
    "ADMIN_BLACKLIST_EXEMPT" => true, // If true, any user who is an admin will not be effected by the blacklists
);
// Configuration ends here.  You do not need to modify anything past this point

// Declarations
$doctypes = array(
	"plain" => array("Text Document", null),
	"php" => array("PHP Script", null),
	"html4" => array("HTML 4.01", "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">"),
	"html4trans" => array("HTML 4.01 Transitional", "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">"),
	"html4frame" => array("HTML 4.01 Frameset", "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Frameset//EN\" \"http://www.w3.org/TR/html4/frameset.dtd\">"),
	"xhtml1" => array("XHTML 1.0 Strict", "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"),
	"xhtml1trans" => array("XHTML 1.0 Transitional", "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"),
	"xhtml1frame" => array("XHTML 1.0 Frameset", "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Frameset//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd\">"),
	"xhtml11" => array("XHTML 1.1", "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">"),
	"html5" => array("HTML5", "<!DOCTYPE html>")
);
$ext_blacklist = explode(",", get_config_var("EXT_BLACKLIST"));
$file_blacklist = explode(",", get_config_var("FILE_BLACKLIST"));
if(get_config_var("IS_BLACKLISTED"))
{
	$file_blacklist[] = basename(__FILE__);
}

if(get_config_var("OVERRIDE_MAX_UPLOAD") && get_config_var("MAX_UPLOAD") > 0)
{
	foreach(array("post_max_size", "upload_max_filesize") as $val)
	{
		ini_set($val, (int)max(ini_get($val), get_config_var("MAX_UPLOAD")));
	}
}

define("GFM_VERSION", "1.0.1");

// Classes
class zipfile
{
	/*
		zipfile class, for reading or writing .zip files
		Based on tutorial given by John Coggeshall at http://www.zend.com/zend/spotlight/creating-zip-files3.php
		Copyright (C) Joshua Townsend and licensed under the GPL
		Version 1.0
	*/
	var $datasec = array(); // array to store compressed data
	var $files = array(); // array of uncompressed files
	var $dirs = array(); // array of directories that have been created already
	var $ctrl_dir = array(); // central directory
	var $eof_ctrl_dir = "\x50\x4b\x05\x06\x00\x00\x00\x00"; //end of Central directory record
	var $old_offset = 0;
	var $basedir = ".";

	function create_dir($name) // Adds a directory to the zip with the name $name
	{
		$name = str_replace("\\", "/", $name);

		$fr = "\x50\x4b\x03\x04";
		$fr .= "\x0a\x00";	// version needed to extract
		$fr .= "\x00\x00";	// general purpose bit flag
		$fr .= "\x00\x00";	// compression method
		$fr .= "\x00\x00\x00\x00"; // last mod time and date

		$fr .= pack("V",0); // crc32
		$fr .= pack("V",0); //compressed filesize
		$fr .= pack("V",0); //uncompressed filesize
		$fr .= pack("v",strlen($name)); //length of pathname
		$fr .= pack("v", 0); //extra field length
		$fr .= $name;
		// end of "local file header" segment

		// no "file data" segment for path

		// "data descriptor" segment (optional but necessary if archive is not served as file)
		$fr .= pack("V",0); //crc32
		$fr .= pack("V",0); //compressed filesize
		$fr .= pack("V",0); //uncompressed filesize

		// add this entry to array
		$this->datasec[] = $fr;

		$new_offset = strlen(implode("", $this->datasec));

		// ext. file attributes mirrors MS-DOS directory attr byte, detailed
		// at http://support.microsoft.com/support/kb/articles/Q125/0/19.asp

		// now add to central record
		$cdrec = "\x50\x4b\x01\x02";
		$cdrec .="\x00\x00";	// version made by
		$cdrec .="\x0a\x00";	// version needed to extract
		$cdrec .="\x00\x00";	// general purpose bit flag
		$cdrec .="\x00\x00";	// compression method
		$cdrec .="\x00\x00\x00\x00"; // last mod time and date
		$cdrec .= pack("V",0); // crc32
		$cdrec .= pack("V",0); //compressed filesize
		$cdrec .= pack("V",0); //uncompressed filesize
		$cdrec .= pack("v", strlen($name) ); //length of filename
		$cdrec .= pack("v", 0 ); //extra field length
		$cdrec .= pack("v", 0 ); //file comment length
		$cdrec .= pack("v", 0 ); //disk number start
		$cdrec .= pack("v", 0 ); //internal file attributes
		$cdrec .= pack("V", 16 ); //external file attributes  - 'directory' bit set

		$cdrec .= pack("V", $this->old_offset); //relative offset of local header
		$this->old_offset = $new_offset;

		$cdrec .= $name;
		// optional extra field, file comment goes here
		// save to array
		$this->ctrl_dir[] = $cdrec;
	}


	function create_file($data, $name) // Adds a file to the path specified by $name with the contents $data
	{
		$name = str_replace("\\", "/", $name);

		$fr = "\x50\x4b\x03\x04";
		$fr .= "\x14\x00";	// version needed to extract
		$fr .= "\x00\x00";	// general purpose bit flag
		$fr .= "\x08\x00";	// compression method
		$fr .= "\x00\x00\x00\x00"; // last mod time and date

		$unc_len = strlen($data);
		$crc = crc32($data);
		$zdata = gzcompress($data);
		$zdata = substr($zdata, 2, -4); // fix crc bug
		$c_len = strlen($zdata);
		$fr .= pack("V",$crc); // crc32
		$fr .= pack("V",$c_len); //compressed filesize
		$fr .= pack("V",$unc_len); //uncompressed filesize
		$fr .= pack("v", strlen($name) ); //length of filename
		$fr .= pack("v", 0 ); //extra field length
		$fr .= $name;
		// end of "local file header" segment

		// "file data" segment
		$fr .= $zdata;

		// "data descriptor" segment (optional but necessary if archive is not served as file)
		$fr .= pack("V",$crc); // crc32
		$fr .= pack("V",$c_len); // compressed filesize
		$fr .= pack("V",$unc_len); // uncompressed filesize

		// add this entry to array
		$this->datasec[] = $fr;

		$new_offset = strlen(implode("", $this->datasec));

		// now add to central directory record
		$cdrec = "\x50\x4b\x01\x02";
		$cdrec .="\x00\x00";	// version made by
		$cdrec .="\x14\x00";	// version needed to extract
		$cdrec .="\x00\x00";	// general purpose bit flag
		$cdrec .="\x08\x00";	// compression method
		$cdrec .="\x00\x00\x00\x00"; // last mod time & date
		$cdrec .= pack("V",$crc); // crc32
		$cdrec .= pack("V",$c_len); //compressed filesize
		$cdrec .= pack("V",$unc_len); //uncompressed filesize
		$cdrec .= pack("v", strlen($name) ); //length of filename
		$cdrec .= pack("v", 0 ); //extra field length
		$cdrec .= pack("v", 0 ); //file comment length
		$cdrec .= pack("v", 0 ); //disk number start
		$cdrec .= pack("v", 0 ); //internal file attributes
		$cdrec .= pack("V", 32 ); //external file attributes - 'archive' bit set

		$cdrec .= pack("V", $this->old_offset); //relative offset of local header
		$this->old_offset = $new_offset;

		$cdrec .= $name;
		// optional extra field, file comment goes here
		// save to central directory
		$this->ctrl_dir[] = $cdrec;
	}

	function read_zip($name)
	{
		// Clear current file
		$this->datasec = array();

		// File information
		$this->name = $name;
		$this->mtime = filemtime($name);
		$this->size = filesize($name);

		// Read file
		$fh = fopen($name, "rb");
		$filedata = fread($fh, $this->size);
		fclose($fh);
//echo 'read_zip open';var_export($filedata);exit;
		// Break into sections
		$filesecta = explode("\x50\x4b\x05\x06", $filedata);

		// ZIP Comment
		$unpackeda = unpack('x16/v1length', $filesecta[1]);
		$this->comment = substr($filesecta[1], 18, $unpackeda['length']);
		$this->comment = str_replace(array("\r\n", "\r"), "\n", $this->comment); // CR + LF and CR -> LF

		// Cut entries from the central directory
		$filesecta = explode("\x50\x4b\x01\x02", $filedata);
		$filesecta = explode("\x50\x4b\x03\x04", $filesecta[0]);
		array_shift($filesecta); // Removes empty entry/signature

		foreach($filesecta as $filedata)
		{
			// CRC:crc, FD:file date, FT: file time, CM: compression method, GPF: general purpose flag, VN: version needed, CS: compressed size, UCS: uncompressed size, FNL: filename length
			$entrya = array();
			$entrya['error'] = "";

			$unpackeda = unpack("v1version/v1general_purpose/v1compress_method/v1file_time/v1file_date/V1crc/V1size_compressed/V1size_uncompressed/v1filename_length", $filedata);

			// Check for encryption
			$isencrypted = (($unpackeda['general_purpose'] & 0x0001) ? true : false);

			// Check for value block after compressed data
			if($unpackeda['general_purpose'] & 0x0008)
			{
				$unpackeda2 = unpack("V1crc/V1size_compressed/V1size_uncompressed", substr($filedata, -12));

				$unpackeda['crc'] = $unpackeda2['crc'];
				$unpackeda['size_compressed'] = $unpackeda2['size_uncompressed'];
				$unpackeda['size_uncompressed'] = $unpackeda2['size_uncompressed'];

				unset($unpackeda2);
			}

			$entrya['name'] = substr($filedata, 26, $unpackeda['filename_length']);

			if(substr($entrya['name'], -1) == "/") // skip directories
			{
				continue;
			}

			$entrya['dir'] = dirname($entrya['name']);
			$entrya['dir'] = ($entrya['dir'] == "." ? "" : $entrya['dir']);
			$entrya['name'] = basename($entrya['name']);

			$filedata = substr($filedata, 26 + $unpackeda['filename_length']);

			if(strlen($filedata) != $unpackeda['size_compressed'])
			{
				$entrya['error'] .= "Compressed size is not equal to the value given in header. ";
			}

			if($isencrypted)
			{
				$entrya['error'] .= "Encryption is not supported. ";
			}
			else
			{
				switch($unpackeda['compress_method'])
				{
					case 0: // Stored
						// Not compressed, continue
					break;
					case 8: // Deflated
						echo PHP_EOL.'Unzip Deflated Report of '.$entrya['dir'].'/'.$entrya['name'].':'.PHP_EOL.'Size before: '.strlen($filedata).'. ';//Reporting
						$filedata = gzinflate($filedata);//$filedata = gzinflate($filedata);#test unzip
						echo 'Size after gzinflate: '.strlen($filedata).'.<br />'.PHP_EOL;//Reporting. todo? store $filedata in file? #dbg
						$entrya['error'] .= $filedata?'':"Warning: gzinflate(): data error (ver:{$unpackeda['version']})! Compression method ({$unpackeda['compress_method']}) in error. ".PHP_EOL.'Filedata:'.PHP_EOL.$filedata;//1.0.1
					break;
					case 12: // BZIP2
						if(!extension_loaded("bz2"))
						{
							@dl((strtolower(substr(PHP_OS, 0, 3)) == "win") ? "php_bz2.dll" : "bz2.so");
						}

						if(extension_loaded("bz2"))
						{
							$filedata = bzdecompress($filedata);
						}
						else
						{
							$entrya['error'] .= "Required BZIP2 Extension not available. ";
						}
					break;
					default:
						$entrya['error'] .= "Compression method ({$unpackeda['compress_method']}) not supported. ";
				}

				if(!$entrya['error'])
				{
					if($filedata === false)
					{
						$entrya['error'] .= "Decompression failed. ";
					}
					elseif(strlen($filedata) != $unpackeda['size_uncompressed'])
					{
						$entrya['error'] .= "File size is not equal to the value given in header. ";
					}
					elseif(crc32($filedata) != $unpackeda['crc'])
					{
						$entrya['error'] .= "CRC32 checksum is not equal to the value given in header. ";
					}
				}

				$entrya['filemtime'] = mktime(($unpackeda['file_time']  & 0xf800) >> 11,($unpackeda['file_time']  & 0x07e0) >>  5, ($unpackeda['file_time']  & 0x001f) <<  1, ($unpackeda['file_date']  & 0x01e0) >>  5, ($unpackeda['file_date']  & 0x001f), (($unpackeda['file_date'] & 0xfe00) >>  9) + 1980);
				$entrya['data'] = $filedata;
			}

			$this->files[] = $entrya;
		}

		return $this->files;
	}

	function add_file($file, $dir = ".", $file_blacklist = array(), $ext_blacklist = array())
	{
		$file = str_replace("\\", "/", $file);
		$dir = str_replace("\\", "/", $dir);

		if(strpos($file, "/") !== false)
		{
			$dira = explode("/", "{$dir}/{$file}");
			$file = array_shift($dira);
			$dir = implode("/", $dira);
			unset($dira);
		}

		while(substr($dir, 0, 2) == "./")
		{
			$dir = substr($dir, 2);
		}
		while(substr($file, 0, 2) == "./")
		{
			$file = substr($file, 2);
		}
		if(!in_array($dir, $this->dirs))
		{/*
			if($dir == ".")
			{
				$this->create_dir("./");
			}*/
			$this->dirs[] = $dir;
		}
		if(in_array($file, $file_blacklist))
		{
			return true;
		}
		foreach($ext_blacklist as $ext)
		{
			if(substr($file, -1 - strlen($ext)) == ".{$ext}")
			{
				return true;
			}
		}

		$filepath = (($dir && $dir != ".") ? "{$dir}/" : "").$file;
		if(is_dir("{$this->basedir}/{$filepath}"))
		{
			$dh = opendir("{$this->basedir}/{$filepath}");
			while(($subfile = readdir($dh)) !== false)
			{
				if($subfile != "." && $subfile != "..")
				{
					$this->add_file($subfile, $filepath, $file_blacklist, $ext_blacklist);
				}
			}
			closedir($dh);
		}
		else
		{
			$this->create_file(implode("", file("{$this->basedir}/{$filepath}")), $filepath);
		}

		return true;
	}

	function zipped_file() // return zipped file contents
	{
		$data = implode("", $this->datasec);
		$ctrldir = implode("", $this->ctrl_dir);

		return $data.
				$ctrldir.
				$this->eof_ctrl_dir.
				pack("v", sizeof($this->ctrl_dir)). // total number of entries "on this disk"
				pack("v", sizeof($this->ctrl_dir)). // total number of entries overall
				pack("V", strlen($ctrldir)). // size of central dir
				pack("V", strlen($data)). // offset to start of central dir
				"\x00\x00"; // .zip file comment length
	}
}

// Functions
function error($str, $redirect = null)
{
	gfm_session("lasterror", $str);

	redirect($redirect);
}

function get_config_var($var)
{
    global $aConfig;
    return $aConfig[$var];
}

function gfmlink($text, $query = "", $attrs = "", $hidedir = false)
{
	global $self, $subdir;

	if(!$hidedir && $subdir && (strpos($query, "dir=") === false))
	{
		$query .= ($query ? "&amp;" : "")."dir=".rawurlencode($subdir);
	}

	return "<a href=\"{$self}".($query ? "?{$query}" : "")."\"".($attrs ? " {$attrs}" : "").">{$text}</a>";
}

function gfm_session()
{
	$args = func_get_args();
	$argc = func_num_args();

	if(func_num_args() == 1)
	{
		$arg = $args[0];
		if(is_array($arg))
		{
			foreach($arg as $key => $val)
			{
				$_SESSION['gfm'][$key] = $val;
			}
		}
		else
		{
			return @$_SESSION['gfm'][$arg];
		}
	}
	else
	{
		for($x = 0; $x < $argc; $x += 2)
		{
			@$_SESSION['gfm'][$args[$x]] = $args[$x + 1];
		}
	}

	return $_SESSION['gfm'];
}
//stackoverflow.com/questions/1175096/ddg#2886224
function isSecure()
{
  return
    (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
    || $_SERVER['SERVER_PORT'] == 443;
}

function redirect($query = "", $append = false)
{
	global $self, $subdir, $file, $action;

	gfm_session("lastfile", $file, "lastaction", $action);

	if(!func_num_args() || $append)
	{
		$query = "action=default".($subdir ? "&dir={$subdir}" : "").($query ? "&{$query}" : "");
	}
	$query = ((substr($query, 0, 1) == "?" || !$query) ? $query : "?{$query}");

	header("Location: http".(isSecure()?'s':'')."://{$_SERVER['HTTP_HOST']}{$_SERVER['PHP_SELF']}{$query}");
	exit;
}

function check_file($file, $skip_exist_check = false)
{
	global $dir, $subdir, $ext_blacklist, $file_blacklist;

	$err = false;
	if(!$file)
	{
		$err = "No filename specified";
	}
	elseif(!$skip_exist_check && !file_exists("{$dir}/{$file}"))
	{
		$err = "File \"".htmlspecialchars($file)."\" does not exist.";
	}
	elseif(!$skip_exist_check && !is_readable("{$dir}/{$file}"))
	{
		$err = "Item \"{$dir}/{$file}\" is not a readable.";
	}
	if($ext = ext_is_blacklisted($file))
	{
		$err = "\"{$ext}\" is on the extension blacklist.";
	}

	if(file_is_blacklisted($file))
	{
		$err = "\"{$file}\" is on the filename blacklist.";
	}

	if(substr_count($file, "../") > (substr_count($subdir, "/") + ($subdir ? 1 : 0)))
	{
		$err = "\"{$file}\" points to an invalid target directory.";
	}

	return $err;
}

function ext_is_blacklisted($file)
{
	global $ext_blacklist;

	foreach($ext_blacklist as $ext)
	{
		if(substr($file, -1 - strlen($ext)) == ".{$ext}")
		{
			return $ext;
		}
	}

	return false;
}

function file_is_blacklisted($file)
{
	global $file_blacklist;

	return in_array($file, $file_blacklist);
}

function rename_files_folder_in_mass($pathname, $replace, $to, $mode = '')//http://www.phpbuilder.com/snippet/detail.php?type=snippet&id=895 1st launch
{
 $res = FALSE;
 $dir=opendir($pathname);
 while ($file2 = readdir($dir))
 {
  if ($file2 != ".." AND $file2 != ".")
  {
   $new = str_replace($replace, $to, $file2);
   if(strstr($mode,'preg_replace'))
   {
    $new = preg_replace('~'.$replace.'~', $to, $file2);
   }
   if(strstr($mode,'strtolower'))
   {
    $new = strtolower($new);
   }
   if(strstr($mode,'strtoupper'))
   {
    $new = strtoupper($new);
   }
   $res = rename($pathname.$file2, $pathname.$new);
   if($res AND strstr($mode,'recursive') AND is_dir($pathname.$new))
   {
    $res = rename_files_folder_in_mass($pathname.$new.'/', $replace, $to, $mode);
   }
  }
 }
 closedir($dir);
 return $res;
}

function mkdir_recursive($pathname, $mode)//4 unzip
{
	is_dir(dirname($pathname)) || mkdir_recursive(dirname($pathname), $mode);
	return is_dir($pathname) || @mkdir($pathname, $mode);
}

/**
 * Copy a file, or recursively copy a folder and its contents
 * @author Aidan Lister <aidan@php.net>
 * @version 1.0.1-gfm
 * @link http://aidanlister.com/2004/04/recursively-copying-directories-in-php/
 * @param       string   $source    Source path
 * @param       string   $dest      Destination path
 * @return      bool     Returns TRUE on success, FALSE on failure
 */
function copyr($source, $dest)
{
	if (is_link($source)) {// Check for symlinks
		return symlink(readlink($source), $dest);
	}
	if (is_file($source)) {// Simple copy for a file
		return copy($source, $dest);
	}
	if (!is_dir($dest)) {// Make destination directory
		mkdir($dest, get_config_var("DEFAULT_PERMS"));
	}
	$dir = dir($source);
	while (false !== $entry = $dir->read()) {// Loop through the folder
		if ($entry == '.' || $entry == '..') {// Skip pointers
			continue;
		}
		if(copyr("{$source}/{$entry}", "{$dest}/{$entry}")){// Deep copy directories
			@chmod("{$dest}/{$entry}", get_config_var("DEFAULT_PERMS"));//4 gfm
		}
	}
	$dir->close();// Clean up
	return true;
}
//trying to delete recursively a tree : nbari at dalmp dot com : http://php.net/manual/en/function.rmdir.php#110489 & http://les.pages.perso.chez.free.fr/suppression-de-fichiers-et-dossiers-impossible.io
function delTree($dir)
{
	$files = array_diff(scandir($dir), array('.','..'));
	foreach ($files as $file)
	{
		(is_dir("{$dir}/{$file}")) ? delTree("{$dir}/{$file}") : unlink("{$dir}/{$file}");
	}
	$deleted = @rmdir($dir);// (effacement normal ailleurs que chez Free)
	if (is_dir($dir)) {// l'effacement a échoué
		$deleted = rename($dir,get_config_var("DIR").'.trash_me');// rename "spécial Free" rename empty folders to /.trash_me (effet de bord non garanti de rename)
	}
	return $deleted;
}
// File manipulation/information functions
function xhtml_highlight_file($file)
{
	if (version_compare(phpversion(), "4.2.0") == "-1")
	{
		return htmlspecialchars(implode('', file($file)));
	}
	else
	{
//		return str_replace(array("</font>", "<font color=\""), array("</span>", "<span style=\"color: "), highlight_file($file, TRUE));//1.0
//php.net/manual/en/function.highlight-file.php#92160  + lines numbers
		$code = substr(highlight_file($file, true), 36, -15);//Strip code and first span
		$lines = explode('<br />', $code);//Split lines
		$lineCount = count($lines);//Count
		$padLength = strlen($lineCount+1);//Calc pad length
		$ret = "<code><span style=\"color: #000000\">";//Re-Print the code and span again
		foreach($lines as $i => $line) {//Loop lines
			$lineNumber = str_pad($i + 1,  $padLength, '0', STR_PAD_LEFT);//Create line number
			$ret .= sprintf('<br /><span style="color: #999999">%s | </span>%s', $lineNumber, $line);//Print line
		}
		return $ret ."</span></code>";//Close span
	}
}

function dirsize($file, $depth = -1)
{
	$size = 0;
	if(!is_readable($file))
	{
		trigger_error(__FUNCTION__."(): Specified file \"{$file}\" is not a readable file or directory", E_USER_WARNING);
		return false;
	}

	if(is_dir($file))
	{
		$dh = opendir($file);
		while(($subfile = readdir($dh)) !== false)
		{
			if($subfile != "." && $subfile != "..")
			{
				if(is_dir("{$file}/{$subfile}"))
				{
					if($depth != 0)
					{
						$size += dirsize("{$file}/{$subfile}", ($depth - 1));
					}
				}
				else
				{
					$size += filesize("{$file}/{$subfile}");
				}
			}
		}
		closedir($dh);
	}
	else
	{
		$size = filesize($file);
	}

	return $size;
}

function resolvepath($path, $incdot = false)
{
	$path = str_replace("\\", "/", $path);
	if($path == "/")
	{
		return "/";
	}
	$patha = explode('/', $path);
	$resolveda = array();
	for ($i = 0; $i < count($patha); $i++)
	{
		if ((!$patha[$i] && $i > 0) || $patha[$i] == ".")
		{
			continue;
		}
		if ($patha[$i] == ".." && $i > 0 && $resolveda[count($resolveda) - 1] != ".." && $resolveda)
		{
			array_pop($resolveda);
			continue;
		}
		array_push($resolveda, $patha[$i]);
	}

	if($patha[0] == "." && !$resolveda)
	{
		array_unshift($resolveda, ".");
	}

	if($incdot && $resolveda[0] != "." && $resolveda[0] != "..")
	{
		array_unshift($resolveda, ".");
	}

	return implode('/', $resolveda);
}

function pathtourl($path)
{
	$root = str_replace("\\", "/", realpath($_SERVER['DOCUMENT_ROOT']))."/";

	$path = realpath($path);
	if($path === false)
	{
		return false;
	}
	$path = str_replace("\\", "/", $path);

	$rootlen = strlen($root);

	return ((substr($path, 0, $rootlen) == $root) ? substr($path, $rootlen - 1) : false);
}

function set_req_var($varname, $value = null)
{
	if(func_num_args() > 1)
	{
		$_REQUEST[$varname] = $_GET[$varname] = $_POST[$varname] = $_COOKIE[$varname] = $value;
	}

	return @$_REQUEST[$varname];
}

// string bytestostr(int $size)
// Returns a string that represents the amount of bytes in the most convenient unit (gb, mb, kb, or b)
function bytestostr($size)
{
	static $bytetypes = array('b', 'kb', 'mb', 'gb');
	foreach(array_reverse($bytetypes, true) as $power => $unit)
	{
		$cursize = ($size / pow(1024, $power));
		if($cursize >= .1)
		{
			break;
		}
	}

	return substr($cursize, 0, 4)."&nbsp;{$unit}";
}

// int byteval(string $size)
// Returns the number of bytes represented by $size.  $size should most likely be from php.ini or from the return value of a previous bytestostr() call.
function byteval($size)
{
	$size = preg_replace("/[^0-9gmk]/", "", strtolower($size));
	$bytes = intval($size);
	switch(substr($size, -1))
	{
		case "g":
			$bytes *= 1024; // Passthrough
		case "m":
			$bytes *= 1024; // Passthrough
		case "k":
			$bytes *= 1024;
	}

	return $bytes;
}

function stripslashes_array($arr)
{
	return (is_array($arr) ? array_map('stripslashes_array', $arr) : (is_string($arr) ? stripslashes($arr) : $arr));
}

if(get_magic_quotes_gpc())
{
	foreach(array('_GET', '_POST', '_COOKIE', '_SERVER', '_ENV', '_REQUEST') as $__s)
	{
		${$__s} = stripslashes_array(${$__s});
		unset($__s);
	}
}

function page_ob_handler($content)
{
	global $pagetitle, $self, $subdir, $ishtml, $action;

	if(!$ishtml)
	{
		return false;
	}
	$logged = gfm_session('username') . gfm_session('password');
	$action = empty($logged)? 'default': $action;#Fix error when unlogged & when post edit (reload) call editarea on login form
	#http://sudwebdesign.free.fr/plugins/gfm/EditArea/edit_area/edit_area_full.js.anonymous.php (never work in 2020 ;/ by CORS policy
	#https://cdn.jsdelivr.net/gh/cdolivet/EditArea@master/edit_area/edit_area_full.js
#Doc  PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\"<html <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\">
	$content = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">
	<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\">
	<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
	<title>GFM - {$_SERVER['HTTP_HOST']}".($subdir ? " — dir: ".htmlspecialchars($subdir) : "")." — {$self}</title>
	<meta name=\"robots\" content=\"noindex, nofollow\" />
	<meta name=\"viewport\" content=\"width=device-width, user-scalable=yes, initial-scale=1.0\"  />
	<link rel=\"home\" title=\"Home\" href=\"{$self}\" />
	<link rel=\"help\" title=\"Help\" href=\"{$self}?action=help\" />
	<link rel=\"author\" title=\"About the Author\" href=\"http://www.gamingg.net/\" />
	<link rel=\"stylesheet\" href=\"{$self}?action=css\" type=\"text/css\" />
	<script type=\"text/javascript\" src=\"{$self}?action=javascript".($subdir ? "&amp;dir=".htmlspecialchars($subdir) : "")."\"></script>".
($action == 'edit' ? "//EditArea
 <script type=\"text/javascript\" src=\"../EditArea/edit_area/edit_area_full.js\"></script>
 <script type=\"text/javascript\">
//<![CDATA[
 if(window.editAreaLoader){
  var type_allow = 'basic,brainfuck,c,coldfusion,cpp,css,html,java,js,pas,perl,php,python,ruby,robotstxt,sql,tsql,vb,xml'.split(',');//syntax_selection_allow
  var file_type = \"".$pagetitle."\".split('.').pop();
  if(type_allow.indexOf(file_type) != -1)//if type of code is recognized
   file_type = file_type;
  else
   file_type = '';

  editAreaLoader.init({
   id: 'filedata'			// textarea id
   ,syntax: file_type			// syntax to be uses for highgliting
   ,start_highlight: true		// to display with highlight mode on start-up
   ,save_callback: 'save_EditArea'
   ,language: 'en'
// ,display: 'later'			// default: onload
// ,min_width: '100%'			// default 400
   ,EA_load_callback: 'gfm_frame_init'	// default '' , For change width px to 100%
   ,toolbar:'save, syntax_selection, | ,search, go_to_line, fullscreen, |, undo, redo, |, select_font, |, change_smooth_selection, highlight, reset_highlight, word_wrap, |, help'
  });

  function gfm_frame_init(id){// change width px to 100%
// console.log(id);
   var eaFrame = document.getElementById('frame_'+id);//frame_filedata
   if(eaFrame){
    eaFrame.style.width='100%';
    eaFrame.classLock = true;
    //fix search/replace inputs
    var css = 'input[type=\"text\"]{box-sizing:border-box;width:100%;}',
        style = document.createElement('style');
    style.type = 'text/css';
    style.appendChild(document.createTextNode(css));
    frames['frame_filedata'].document.head.appendChild(style);
   }
  }

  function save_EditArea(id, content){// save callback function
   document.getElementById('continue').click();
  }

  function fix_save_btn(){
   document.getElementById('formedit').onsubmit = function(){
    if(eAs.filedata.displayed)
     eAL.toggle('filedata');
    return true;
   }
   // Fix to be submited without error with 0.8.2 master : line 14 formObj.elements.length ::: formObj is undefined (in edit_area_loader.js line 795)
   // Remove increminated events on forms inspired of EditAreaLoader.prototype window_loaded funk (in edit_area_loader.js)
   if (document.forms) {
    for (var i=0; i<document.forms.length; i++) {
     var form = document.forms[i];
     form.edit_area_replaced_submit=null;
     editAreaLoader.remove_event(form, 'submit', EditAreaLoader.prototype.submit);
     editAreaLoader.remove_event(form, 'reset', EditAreaLoader.prototype.reset);
    }
   }
  }
  window.onload=fix_save_btn;

 }
//]]>
  </script>
" : "")."
	</head>
	<body>
	<h1 class=\"pagetitle\">".((@$pagetitle) ? $pagetitle : "Debug: No Page Title specified")."    {$file}</h1>
	<div id=\"container\">

	{$content}

	<p><a href=\"http://www.gamingg.net/gfm/\">Galaxy File Manager</a> ".GFM_VERSION."
	<span id=\"togglecopy;\">".gfmlink("&copy;", "action=gpl", "onclick=\"toggleFileMenu(this, 1); return false;\" class=\"bullet\"")."</span></p>
	<div id=\"copy\" style=\"float:right;display:none;\">
		<ul class=\"plainlist\">
			<li><em>Copyrights:</em></li>
			<li>2006-2007, 2008 <a href=\"http://www.gamingg.net/\">Joshua Townsend</a>.</li>
			<li>2018-2020 <a href=\"http://sudwebdesign.free.fr/\">Thomas Ingles</a>.</li>
		</ul>
	</div>
 </div>
	</body>
</html>";

	header("X-Uncompressed-Length: ".strlen($content));
	if(($gzcontent = ob_gzhandler($content, PHP_OUTPUT_HANDLER_START | PHP_OUTPUT_HANDLER_END)) !== false)
	{
		$content = $gzcontent;
		header("Content-Length: ".strlen($content));
	}
	unset($gzcontent);

	return $content;
}

// End: Functions

ob_start("page_ob_handler");

$ishtml = true;
$ctype = "text/html";
if(stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml"))
{
	$ctype = "application/xhtml+xml";
}
header("Content-type: {$ctype};charset=utf-8");
header('Access-Control-Allow-Origin: *');

$self = $_SERVER['PHP_SELF'];
$action = ((@$_POST['action']) ? $_POST['action'] : @$_GET['action']);

#session_start();//origin 1.0

if(!isset($_SESSION['gfm']))
{
	$_SESSION['gfm'] = array();
}

$ses_user = gfm_session("username");
$ses_pass = gfm_session("password");
$post_user = @$_POST['username'];
$post_pass = @$_POST['password'];

$users = get_config_var("USERS");

if((!array_key_exists($ses_user, $users) || (@$users[$ses_user]['password'] != $ses_pass && @$users[$ses_user]['passenc'] != md5($ses_pass))) && $action != "css" && $action != "javascript")
{
	if($action == "login")
	{
		if($users[$post_user]['password'] != $post_pass && $users[$post_user]['passenc'] != md5($post_pass))
		{
			error("Login Error", "error");#old redirect("error");
		}
		else
		{
			gfm_session("username", $post_user, "password", $post_pass);
			$oldQuery = (isset($_SERVER["QUERY_STRING"]) && strpos($_SERVER["QUERY_STRING"], "oldaction") !== false)? strtr($_SERVER["QUERY_STRING"], array("oldaction=" => "action=")): null;
			redirect($oldQuery);
		}
	}
#2020.03
	$pagetitle = "Login";
	if(array_key_exists("error", @$_GET)){
	    $pagetitle = gfm_session("lasterror");
	    unset($_GET["error"]);
	    gfm_session("lasterror", "");
	}
	$oldQuery = '';
	if(isset($_SERVER["QUERY_STRING"]) && strpos($_SERVER["QUERY_STRING"], "action") !== false){
	    $oldQuery .= "?oldaction=" . (!isset($_GET["oldaction"])? $_GET["action"]: $_GET["oldaction"]);
	    unset($_GET["action"]);
	    if(!empty($_GET))
	     foreach($_GET as $ok => $ov)
	      $oldQuery .= "&amp;" . $ok . "=" . $ov;
	}
#2020.03
	echo "<form action=\"{$self}{$oldQuery}\" method=\"post\" id=\"login\">
  <fieldset class=\"noborder\">
    <input type=\"hidden\" name=\"action\" value=\"login\" />
    <label for=\"username\">Name</label>: <input type=\"text\" name=\"username\" id=\"username\" />
    <label for=\"password\">Password</label>: <input type=\"password\" name=\"password\" id=\"password\" />
    <input type=\"submit\" value=\"Login\" />
  </fieldset>
</form>";
	exit;
}

if(@$users[$ses_user]['admin'])
{
	if(get_config_var("ADMIN_BLACKLIST_EXEMPT"))
	{
		$ext_blacklist = array();
		$file_blacklist = array();
	}
}

$dir = get_config_var("DIR");
if(isset($users[$ses_user]['dir']))
{
	$dir = rtrim($dir,'/\\')."/{$users[$ses_user]['dir']}";
}

$subdir = resolvepath(@$_REQUEST['dir']);
if(!$subdir || $subdir == "." || (get_config_var("NO_SYMLINK") && substr(realpath("{$dir}/{$subdir}"), 0, strlen(realpath($dir))) != realpath($dir)))
{
	$subdir = "";
}
else
{
	$dir = rtrim($dir,'/\\')."/{$subdir}";
}
if(!@$_REQUEST['file'] || (get_config_var("NO_SYMLINK") && substr(realpath("{$dir}/{$_REQUEST['file']}"), 0, strlen(realpath($dir))) != realpath($dir)))
{
	set_req_var("file", null);
}
$inputdir = ($subdir ? "<input type=\"hidden\" name=\"dir\" value=\"{$subdir}\" />" : "");
$dir = rtrim($dir,'/\\');//²/Fix

switch($action)
{
	case "phpinfo":
		$ishtml = false;
		ob_end_clean();
		header("Content-Type: text/html");
		ob_start();
		phpinfo();
		$phpinfo = ob_get_contents();
		ob_end_clean();

		$phpinfo = str_replace(array("</style>", "<body>"), array("
#gfmlink {
    position: absolute;
    left: 5px;
    top: 5px;
    font-size: .8em;
    font-family: Verdana, sans-serif;
}
</style>", "<body>".gfmlink("Back to Galaxy File Manager", "action=default", "id=\"gfmlink\"")), $phpinfo);
		exit($phpinfo);
		break;
	case "logout":
		unset($_SESSION['gfm']);
		redirect(null);
	break;
	case "css":
		$ishtml = false;
		ob_end_clean();
		header("Content-type: text/css");
		echo "/* Galaxy File Manager CSS - http://www.gamingg.net */
/* Main */
html,body {
/*  width:100%;padding:0; */
  min-height:96.3%;
  margin:0;
}
html,body,td {
  padding: 10px;
  font-family: Verdana, sans-serif;
/*  font-size: 12px; */
  background: #ffffff;
  color: #000000;
}

/* Header */
.pagetitle {
  font-size: 24px;
  font-weight: normal;
  border-bottom: 1px dashed #000000;
  margin-top: 0px;
}

/* Anchors */
a {
  color: #003399;
  text-decoration: none;
}
a.dir {
  font-weight: bold;
}
a:hover {
  text-decoration: underline;
  background: #99cccc;
}
h1 a {
  color: #000000;
}

/* Tables and their cells */
table {
  margin: 20px 0 20px 0;
  border-left: 1px solid #000000;
  border-bottom: 1px solid #000000;
  empty-cells: show;
  cell-spacing: 0;
  border-collapse: collapse;
}
th,td {
  padding: 4px 10px 4px 10px;
  margin: 0px;
}
h2, th {
  font-size: 1.2em;
  font-weight: normal;
  background: #0033aa;
  color: #ffffff;
  text-align: left;
  border: 1px solid #000000;
}
h2 {
  display: block;
  padding: .4em;
}
th {
  border-left: 0;
  min-width: 321px;
}
td {
  border-bottom: 1px solid #000000;
  border-right: 1px solid #000000;
}
tr td:first-child {
  width: 100%;
  max-width: 210px;
  overflow: hidden;
  text-overflow: ellipsis;
  table-layout: fixed;
  white-space: nowrap;
}
td > * {
  overflow: hidden;
  text-overflow: ellipsis;
}
tr td.status {
 max-width: initial;
}
.filecell1 {
  background:#ffffff;
}
.filecell2 {
  background:#ffff99;
}
td.nrb {
  border-right: 0px;
}

/* Navigation */
#navmenu {
/*
  list-style-type: none;
  display: inline;
*/
  margin:0;
  padding:0;
}
#navmenu li {
  display: inline;
}
#navmenu li a {
  color: #000000;
  background: #cccc99;
  text-decoration: none;
  border: 1px solid #000000;
  display: inline-block;
/*  height: 20px; */
  padding: 4px;
  text-align: center;
}
#navmenu li a:hover {
  background: #ffffcc;
  color: #000000;
  text-decoration: underline;
  border: 1px dashed #000000;
}

/* Misc */
.current {
  font-weight: bold;
}
.size {
  font-size: 12px;
}
.small, .options {
  font-size: .8em;
}
.filesource {
  border: #000000 1px dashed;
  padding: 10px;
}
.plainlist, .plainlist li {
  margin: 0;
  padding: 0;
  list-style-type: none;
}
.noborder {
  border: 0;
}
.nospace {
  margin: 0;
  padding: 0;
}
.error {
  color: #FF0000;
}
#control-panel {
  float: left;
  margin-right: 1em;
}
#container{
  min-height: 360px;
}
dt {
  font-weight: bold;
}
@media (max-width: 767px) {
 .plainlist li, .bullet {padding: 0 0.25rem;}
 table{display: inline-table;}
}
";
		exit;
	break;
	case "javascript":
		$ishtml = false;
		ob_end_clean();
		header("Content-type: text/javascript");
?>
// Galaxy File Manager Javascript - http://www.gamingg.net

/*
function doInnerHTML(writeString, writeElement)
{
  try {
    var nodes = new DOMParser().parseFromString("<parse>"+writeString+"</parse>", 'text/xml').documentElement;
    var range = document.createRange();
    range.selectNodeContents(writeElement);
    range.deleteContents();
    for (var i = 0; i < nodes.childNodes.length; i++)
    {
      writeElement.appendChild(document.importNode(nodes.childNodes[i], true));
    }
    return true;
  } catch (e) {
    var range = document.createRange();
    var writeFrag = range.createContextualFragment(writeString);
    return true;
    try {
      writeElement.innerHTML = writeString;
      return true;
    } catch(ee) {
      return false;
    }
  }
}
*/

function parseXMLString(xmlString)
{
  if (window.ActiveXObject)
  {
    var doc = new ActiveXObject("Microsoft.XMLDOM");
    doc.async = "false";
    doc.loadXML(xmlString);
  }
  else
  {
    var parser = new DOMParser();
    var doc = parser.parseFromString(xmlString, "application/xml");
  }

  return doc;
}

function toggleFileMenu(linkRef, type)
{
  fileRef = linkRef.parentNode.parentNode.nextSibling;
  while(fileRef.nodeName == '#text')
  {
    fileRef = fileRef.nextSibling;
  }
  if(type)
  {
   fileRef.style.display = '';
   linkRef.onclick = function()
   {
     toggleFileMenu(this, 0);
     return false;
   }
  }
  else
  {
    fileRef.style.display = 'none';
   linkRef.onclick = function()
   {
     toggleFileMenu(this, 1);
     return false;
   }
  }
}

function showControl(controlId)
{
  return showItem('controls', 'control-list', 'control-'+controlId);
}

function showOption(fileId, fileName, optionId, param1)
{
  var data;
  switch(optionId)
  {
    case 'rename':
      data = new Array('Rename to', fileName, 'Rename');
    break;
    case 'massrename':
      data = new Array('Rename files &amp; folders in mass.<br />From', fileName, 'Replace','To');//Mass Replace
    break;
    case 'chmod':
      data = new Array('Mode', param1, 'Chmod');
    break;
    case 'copy':
      data = new Array('Copy to', 'copy_of_'+fileName, 'Copy');
    break;
    case 'unzip':
      data = new Array('Unzip to Directory', fileName.substr(0, fileName.length - 4), 'Unzip');
    break;
  }
//https://duckduckgo.com/?q=createInputNode&t=lm&ia=qa
  document.getElementById(fileId+'-option-action').value = optionId;
  document.getElementById(fileId+'-option-newvalue').value = data[1];
  document.getElementById(fileId+'-option-submit').value = data[2];
  if(optionId == 'massrename'){//option+
    document.getElementById(fileId+'-option-text-from').innerHTML = data[3];
    document.getElementById(fileId+'-option-replace').value = data[1];
    document.getElementById(fileId+'-option-massrename').style.display = '';
  }else
    document.getElementById(fileId+'-option-massrename').style.display = 'none';

  fileTextObj = document.getElementById(fileId+'-option-text');
  while(fileTextObj.firstChild != undefined)
  {
    fileTextObj.removeChild(fileTextObj.firstChild);
  }
  fileTextObj.innerHTML = data[0];

  document.getElementById(fileId+'-option').style.display = '';

  return false;
}

function showItem(containerId, listId, itemId)
{
  containerRef = document.getElementById(containerId);
  for(var x = 0; x < containerRef.childNodes.length; x++)
  {
    if(containerRef.childNodes[x].nodeType != 1)
    {
      continue;
    }
    if(containerRef.childNodes[x].getAttribute('id') != listId)
    {
      containerRef.childNodes[x].style.display = 'none';
    }
  }
  document.getElementById(itemId).style.display = '';

  return false;
}

window.onerror = function(msg, url, line)
{
  alert(msg+' on line '+line+' url: '+url );
}

<?php
	exit;
	break;
	case "source":
		$file = @$_REQUEST['file'];
		$pagetitle = "Source of ".resolvepath("/{$subdir}/{$file}");
		$content = "<span class=\"options\" style=\"float:right\">[".gfmlink("Logout", "action=logout")."]</span><span class=\"options\">[".gfmlink("Go Back", "action=default")."]</span><br /><br />\n";

		if($err = check_file($file))
		{
			error("Cannot view source because: {$err}");
		}

		$content .= "<div class=\"filesource\">".(filesize("{$dir}/{$file}") ? xhtml_highlight_file("{$dir}/{$file}") : "[File contains no data]")."</div>";
		echo $content;
		exit;
	break;
	case "edit": // All errors should be handled specially here to avoid losing file data, and, thus, error() should not be used in this case (no pun intended)
		$file = @$_REQUEST['file'];
		$pagetitle = "Viewing Source of ".resolvepath("/{$subdir}/{$file}");
		$content = "<span class=\"options\" style=\"float:right\">[".gfmlink("Logout", "action=logout")."]</span><span class=\"options\">[".gfmlink("Go Back", "action=default")."]</span><br /><br />\n";
		$statusmsg = "";
		$to = isset($_REQUEST['action'])? '?action=' . $_REQUEST['action'] . '&amp;file=' . $file: '';

		if($err = check_file($file))
		{
			$statusmsg = "<strong class=\"error\">Error</strong>: {$err}";
		}

		if(array_key_exists("filedata", $_POST))
		{
			$filedata = @$_POST['filedata'];
			if(($fh = @fopen("{$dir}/{$file}", "w")) !== false)
			{
				if(fwrite($fh, $filedata) !== false)
				{
					fclose($fh);
					if(@$_REQUEST['continue'])
					{
						$statusmsg = "File \"".htmlspecialchars($file)."\" has been saved <i>at ".date(get_config_var("DATE_FORMAT"))."</i>.";
						//$to = isset($_REQUEST['action'])? 'action=' . $_REQUEST['action'] . '&file=' . $file: null;
						//redirect($to);#lost status :/
					}
					else
					{
						redirect();
					}

				}
				else
				{
					$statusmsg = "<strong class=\"error\">Error</strong>: Failed writing to file.";
				}
			}
			else
			{
				$statusmsg = "<strong class=\"error\">Error</strong>: Failed opening file; try changing file permissions to allow write access.";
			}
		}
		if(!array_key_exists("filedata", $_POST) || $statusmsg)
		{
			$fcontents = (array_key_exists("filedata", $_POST) ? $_POST['filedata'] : implode("", file("{$dir}/{$file}")));
			$content .= ($statusmsg ? "<table>
  <tr>
    <th class=\"current filename\">Status</th>
  </tr>
  <tr>
    <td class=\"status\">
      {$statusmsg}
    </td>
  </tr>
</table>
" : "")."
<form id=\"formedit\" action=\"{$self}{$to}\" method=\"post\">
  <fieldset class=\"noborder\">
    <textarea id=\"filedata\" cols=\"128\" rows=\"32\" name=\"filedata\">".htmlspecialchars($fcontents, ENT_NOQUOTES, "UTF-8")."</textarea>
    <br />
    <input type=\"submit\" value=\"Save\" />
    <input type=\"submit\" value=\"Save and Continue\" id=\"continue\" name=\"continue\" />
    <input type=\"hidden\" name=\"action\" value=\"edit\" />
    <input type=\"hidden\" name=\"file\" value=\"{$file}\" />
    {$inputdir}
  </fieldset>
</form>";
		}
		echo $content;
		exit;
	break;
	case "new":
		$type = @$_REQUEST['type'];
		$file = @$_REQUEST['filename'];
		if($err = check_file($file, true))
		{
			error("Cannot create file because: {$err}");
		}
		if(file_exists("{$dir}/{$file}"))
		{
			error("Cannot create file because:  \"".htmlspecialchars($file)."\" already exists.");
		}

		$isxhtml = (bool)(substr($type, 0, 5) == "xhtml");
		$xhtsl = ($isxhtml ? " /" : "");
		$validhtml = "<a href=\"http://validator.w3.org/check?uri=referer\">Valid {$doctypes[$type][0]}</a>";

		$fcontents = "";
		if(array_key_exists($type, $doctypes) && $type != "plain" && $type != "php")
		{
			$fcontents = "{$doctypes[$type][1]}
<html".($isxhtml ? " xmlns=\"http://www.w3.org/1999/xhtml\"" : "").">
<head>
  <title>{$doctypes[$type][0]}</title>
  <meta http-equiv=\"content-type\" content=\"".($isxhtml ? "application/xhtml+xml" : "text/html").";charset=utf-8\"{$xhtsl}>
  <meta name=\"generator\" content=\"Galaxy File Manager ".GFM_VERSION."\"{$xhtsl}>
</head>".
((substr($type, -5) == "frame") ? "
<frameset cols=\"*, *\">
  <frame src=\"frame1.html\"{$xhtsl}>
  <frame src=\"frame2.html\"{$xhtsl}>

  <noframes>
    <body>
      <p>
        {$validhtml}
      </p>
    </body>
  </noframes>
</frameset>
" : "
<body>
  <p>
    {$validhtml}
  </p>
</body>
").
"</html>";
		}
		if($type == "php")
		{
			$fcontents = "<?php
phpinfo();
?>";
		}
		if(($fh = fopen("{$dir}/{$file}", "w")) !== false)
		{
			if(fwrite($fh, $fcontents) !== false)
			{
				fclose($fh);
				@chmod("{$dir}/{$file}", get_config_var("DEFAULT_PERMS"));
				redirect();
			}
		}
		error("Error creating file \"".htmlspecialchars($file)."\".  Check to see if a valid filename was chosen.");
	break;
	case "newdir":
		$file = @$_REQUEST['dirname'];
		if($err = check_file($file, true))
		{
			error("Cannot create file because: {$err}");
		}
		if(file_exists("{$dir}/{$file}"))
		{
			error("Cannot create file because:  \"".htmlspecialchars($file)."\" already exsits.");
		}
		if(@mkdir("{$dir}/{$file}", get_config_var("DEFAULT_PERMS")))
		{
			@chmod("{$dir}/{$file}", get_config_var("DEFAULT_PERMS"));
			redirect();
		}
		error("Error creating directory \"".htmlspecialchars($file)."\".  Check to see if a valid filename was chosen.");
	break;
	case "delete":
		$file = @$_REQUEST['file'];

		if($err = check_file($file))
		{
			error("Cannot delete file because: {$err}");
		}
		if((is_dir("{$dir}/{$file}") ? @delTree("{$dir}/{$file}") : @unlink("{$dir}/{$file}")))
		{
			redirect();
		}
		error("Error deleting file \"".htmlspecialchars($file)."\".  Check to see if you have proper access permissions to this file (chmod the file to 777).  ".(is_dir("{$dir}/{$file}") ? "  Remove all files, if any, from the directory before trying again." : ""));
	break;
	case "rename":
		$file = @$_REQUEST['file'];
		$newfile = @$_REQUEST['newvalue'];

		if($err = check_file($file))
		{
			error("Cannot rename file because: {$err}");
		}
		if($err = check_file($newfile, true))
		{
			error("Cannot rename file because: {$err}");
		}
		if(@rename("{$dir}/{$file}", "{$dir}/{$newfile}"))
		{
			redirect();
		}
		error("Error renaming file \"".htmlspecialchars($file)."\" to \"".htmlspecialchars($newfile)."\".  Check to see if valid filenames were chosen.");
	break;
	case "massrename":
		$file = @$_REQUEST['file'];
		$replace = @$_REQUEST['newvalue'];//inversed : new is old (from)
		$to = @$_REQUEST['replace'];
		$mode = isset($_REQUEST['preg_match'])?'preg_match':'';
		$mode.= isset($_REQUEST['strtolower'])?'strtolower':'';
		$mode.= isset($_REQUEST['strtoupper'])?'strtoupper':'';
		$mode.= isset($_REQUEST['recursive'])?'recursive':'';
		if($file AND $err = check_file($file))
		{
			error("Cannot mass rename files because: {$err}");
		}
		$folder = rtrim($dir.'/'.$file.'/','/');
		if(rename_files_folder_in_mass("{$folder}/", "{$replace}", "{$to}", "{$mode}"))
		{
			redirect();
		}
		error("Error renaming \"".htmlspecialchars("{$folder}/")."\" folder files from \"".htmlspecialchars($replace)."\" to \"".htmlspecialchars($to)."\".  Check to see if valid folder were chosen.");
	break;
	case "upload":
		$filea = $_FILES['newfile'];
		$file = (@$_REQUEST['lowercase'] ? strtolower($filea['name']) : $filea['name']);
		if(is_uploaded_file($filea['tmp_name']))
		{
			if($err = check_file($file, true))
			{
				error("Cannot upload file because: {$err}");
			}
			if(file_exists("{$dir}/{$file}"))
			{
				if(@$_REQUEST['overwrite'])
				{
					if(!@unlink("{$dir}/{$file}"))
					{
						error("Cannot upload file because: File \"".htmlspecialchars($file)."\" could not be overwritten.");
					}
				}
				else
				{
					error("Cannot upload file because: File \"".htmlspecialchars($file)."\" is invalid or already exists.");
				}
			}
			if(filesize($filea['tmp_name']) > get_config_var("MAX_UPLOAD") && get_config_var("MAX_UPLOAD") > 0)
			{
				error("Cannot upload file because: File \"".htmlspecialchars($file)."\" is above the allowed upload file size limit.");
			}
			if(@rename($filea['tmp_name'], "{$dir}/{$file}"))
			{
				@chmod("{$dir}/{$file}", get_config_var("DEFAULT_PERMS"));
				redirect();
			}
			else
			{
				error("Cannot upload file because: Unknown error");
			}
		}
		error("Cannot upload file because: Invalid file");
	break;
	case "chmod":
		$file = @$_REQUEST['file'];
		$mode = octdec((int)@$_REQUEST['newvalue']); // Ensure that $mode is octal

		if($err = check_file($file))
		{
			error("Cannot upload file because: {$err}");
		}
		//error("Mode was: ".var_export($_REQUEST['newvalue'], 1).", then ".var_export($mode, 1));
		if(@chmod("{$dir}/{$file}", $mode))
		{
			redirect();
		}
		error("Error chmod'ing file \"".htmlspecialchars($file)."\" to $mode.");
	break;
	case "copy":
		$file = @$_REQUEST['file'];
		$newfile = @$_REQUEST['newvalue'];

		if($err = check_file($file))
		{
			error("Cannot copy file because: {$err}");
		}
		if($err = check_file($newfile, true))
		{
			error("Cannot copy file because: {$err}");
		}
		if(file_exists("{$dir}/{$newfile}"))
		{
			error("Cannot copy file because: ".htmlspecialchars($newfile)." already exists.");
		}
		$fileOrDir = (is_dir("{$dir}/{$file}")) ? "directory" : "file";
		if(!copyr("{$dir}/{$file}","{$dir}/{$newfile}"))
		{
			error("Cannot copy ".htmlspecialchars("{$dir}/{$file}")." ".$fileOrDir." because: ".htmlspecialchars("{$dir}/{$newfile}")." already exists.");
			redirect();
		}
		else
		{
			redirect();
		}
		error("Error copying ".$fileOrDir." \"".htmlspecialchars($file)."\" to \"".htmlspecialchars($newfile)."\".  Check to see if valid ".$fileOrDir." name were chosen.");
	break;
	case "dlzip":
		$file = @$_REQUEST['file'];

		if($err = check_file($file))
		{
			error("Cannot zip file because: {$err}");
		}
		if(substr($file, -4) == ".zip")
		{
			error("Cannot zip file because: File already zipped");
		}

		$ishtml = false;
		$zipfile = new zipfile();
		$zipfile->basedir = $dir;
		$zipfile->add_file($file, ".", $file_blacklist, $ext_blacklist);
		$zipped = $zipfile->zipped_file();
		if (headers_sent())
		{
			echo 'HTTP header already sent';// HTTP header has already been sent
			exit;
		}

		while (ob_get_level() > 0){ob_end_clean();}// clean buffer(s)
		ob_start();
//		header('Pragma: no-cache');
		header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
		header("Content-Transfer-Encoding: Binary");
		header("Content-Type: application/zip");
		header("Content-disposition: attachment;filename=\"".(($file == ".") ? ((realpath($dir) == realpath(get_config_var("DIR"))) ? $ses_user : basename($dir)) : $file).".zip\"");
		header("Content-Length: ".strlen($zipped));//filesize($file)
		ob_flush();
		ob_clean();

		echo $zipped;
		exit;
		break;
	case "unzip":
		$file = @$_REQUEST['file'];
		$targetdir = @$_REQUEST['newvalue'];

		if(class_exists('ZipArchive')){
			//php.net/manual/en/class.ziparchive.php#105312
			$aFile = array();
			$aFile['error'] = "";
			$za = new ZipArchive();
			$res = $za->open("{$dir}/{$file}");
			if ($res === TRUE) {
				for( $i = 0; $i < $za->numFiles; $i++ ){
					$aFile = $za->statIndex( $i );//php.net/manual/en/ziparchive.statindex.php
					if(substr($aFile['name'], -1) == "/") // skip directories
					{
						continue;
					}
					if(file_exists("{$dir}/{$targetdir}{$aFile['name']}"))
					{
						error("Cannot unzip file because: ".htmlspecialchars("{$dir}/{$targetdir}{$aFile['name']}")." already exists.<br />The Unzip command will not override to protect existing files!<br />So delete or rename this file if you really wish to unzip ".htmlspecialchars($file).".");
					}
					$za->extractTo("{$dir}/{$targetdir}", $aFile['name']);//extract one by one
					touch("{$dir}/{$targetdir}/{$aFile['name']}", $aFile['mtime'], time());

				}
				$za->close();
				echo 'ok';
			} else {
				echo 'failed';
			}

			redirect();
			break;
		}

		$zipfile = new zipfile();//Internal class

		foreach($zipfile->read_zip("{$dir}/{$file}") as $aFile)
		{
			$zipdir = "{$dir}/{$targetdir}".($aFile['dir'] ? "/{$aFile['dir']}" : "");
			if(!file_exists($zipdir))
			{
				mkdir_recursive($zipdir, get_config_var("DEFAULT_PERMS"));
			}

			if(file_exists("{$zipdir}/{$aFile['name']}"))
			{
				error("Cannot unzip file because: ".htmlspecialchars("{$targetdir}/{$aFile['name']}")." already exists.<br />The Unzip command will not override to protect existing files!<br />So delete or rename this file if you really wish to unzip ".htmlspecialchars($file).".");
			}

			$fh = fopen("{$zipdir}/{$aFile['name']}", "w");
			fwrite($fh, $aFile['data']);
			fclose($fh);

			touch("{$zipdir}/{$aFile['name']}", $aFile['filemtime'], time());
		}

		redirect();
	break;
	case "help":
		$pagetitle = "Help";

?>
<span class="options">[<?php echo gfmlink("Go Back", "action=default"); ?>]</span><br /><br />
<dl>
    <dt>How do I move files to another directory?</dt>
    <dd>Simply rename the file, including the directory you would like to move it to.  For instance, if you have a file called "foo.png" and you want to move it into the directory called "bar", use the Rename command to rename foo.png to "bar/foo.png".  If you wanted to move a file to the parent directory, use "..".  For instance, if you have a file called "foo.png" and you want to move it to the directory "bar" that is located in the parent directory, use teh Rename command to rename foo.png to "../bar/foo.png".</dd>
    <dt>How do I unzip files to the current directory/parent directory?</dt>
    <dd>".." stands for the parent directory, and "." stands for the current directory.  So, if you wanted to unzip the contents of a ZIP file into the current directory, use the Unzip command and unzip to empty or "." folder.</dd>
    <dt>Where can I get the newest version of Galaxy File Manager?</dt>
    <dd>At my website: <a href="http://sudwebdesign.free.fr/">sudwebdesign.free.fr</a></dd>
    <dd>Or at website of Joshua: <a href="http://www.gamingg.net/">GamingG.net</a></dd>
    <dt>I found a bug, how can I report it?</dt>
    <dd>Send me an e-mail at <a href="mailto:sudwebdesign@free.fr?subject=gfm%20bug">sudwebdesign@free.fr</a>.</dd>
    <dt>I have a suggestion for a future version of Galaxy File Manager, or another program or script that I think you should make.  Would you like to hear it?</dt>
    <dd>Of course!  Just send me an e-mail at <a href="mailto:sudwebdesign@free.fr?subject=gfm%20suggest">sudwebdesign@free.fr</a>.</dd>
    <dt>Who are Joshua, the initial dev of Galaxy File Manager ?</dt>
    <dd>Well, that's a difficult question to answer, isn't it? (:P)  My name is Joshua Townsend, though I use the handle/screen name "GamingG" most of the time.  Because I use the name GamingG for most purposes, a web search for "GamingG" would find much more about me than "Joshua Townsend" would.  I am a programmer who believes in <a href="http://www.gnu.org/philosophy/free-sw.html">free software</a> and, thus, I offer Galaxy File Manager, along with my other programs, as free to use, modify, and redistribute under the <?php echo gfmlink("General Public License", "action=gpl"); ?>.  If you want to modify and/or redistribute it, just remember to keep my copyright statements intact, give credit where credit is due, and follow the other guidelines of the GPL :). if not contact at <a href="mailto:gfm@gamingg.net,sudwebdesign@free.fr?subject=gfm%20unrespected%20GPL">gfm@gamingg.net &amp; sudwebdesign@free.fr</a></dd>
</dl>
<?php
		exit;
	break;
	case "gpl":
		$pagetitle = "General Public License";

		echo "<span class=\"options\">[".gfmlink("Go Back", "action=default")."]</span><br /><br />
<pre>Galaxy File Manager, a web-based file manager
Copyright (C) 2006-2007, 2008 Joshua Townsend
Copyright (C) 2018-2020 Thomas Ingles

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.</pre>

<p>
    The full GPL should have been included in a file called COPYING that was included in the download.  If this file was deleted, the GPL can be viewed here: <a href=\"http://www.gnu.org/licenses/gpl.txt\">http://www.gnu.org/licenses/gpl.txt</a>
</p>
<p>
    If for whatever reason the GPL was not included in the version of Galaxy File Manager that you obtained, please contain programmers at <a href=\"mailto:gfm@gamingg.net,sudwebdesign@free.fr?subject=gfm unrespected GPL\">gfm@gamingg.net &amp; sudwebdesign@free.fr</a>.
</p>";
        exit;
    break;
} // end switch($action)


// $action isn't set, $action is unrecognized, $action == "default", or control was passed to the default action below by not putting "exit;":

echo "You are currently logged in as {$ses_user}.<br /><br />\n\n";

$lastaction = gfm_session("lastaction");
$lastfile = gfm_session("lastfile");
$control = @$_REQUEST['control'];
$option = @$_REQUEST['option'];

$max_upload_size = min(byteval(ini_get("post_max_size")), byteval(ini_get("upload_max_filesize")));
if(get_config_var("MAX_UPLOAD") > 0)
{
	$max_upload_size = min($max_upload_size, (int)get_config_var("MAX_UPLOAD"));
}

$pagetitle = "Contents&nbsp;of: ";
$urldir = $urlsubdir = "";
foreach(explode("/", resolvepath("/{$subdir}")) as $key => $dirpart)
{
	if(!$dirpart && $key !== 0)
	{
		continue;
	}
	$urlsubdir .= ($urlsubdir ? "/" : "").$dirpart;
	$pagetitle .= gfmlink("{$dirpart}/", "action=default&amp;dir={$urlsubdir}");
}
$dirlist  = array();
$filelist = array();
$sizelist = array();
$datelist = array();
$permlist = array();

$totalfiles = 0;
$totalsize = 0;
#https://www.php.net/manual/fr/function.disk-free-space.php#103382 ::: 2019.10.08
$bytes = disk_free_space($dir);#disk_free_space(".");
$si_prefix = array( 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' );
$base = 1024;
$class = min((int)log($bytes , $base) , count($si_prefix) - 1);
$disk_free_space = '<span title="'.$bytes.' bytes">' . sprintf('%1.2f' , $bytes / pow($base,$class)) . ' ' . $si_prefix[$class] . '</span>';

if($dh = @opendir($dir))
{
	while(($file = readdir($dh)) !== false)
	{
		if($file != "." && $file != ".." && !check_file($file))
		{
			if(is_dir("{$dir}/{$file}"))
			{
				$filesize   = 0;
				$numfiles   = 0;
				$numsubdirs = 0;
				$subdh = opendir("{$dir}/{$file}");
				if($subdh)
				{
					while(($subfile = readdir($subdh)) !== false)
					{
						if($subfile != "." && $subfile != "..")
						{
							if(is_dir("{$dir}/{$file}/{$subfile}"))
							{
								$numsubdirs++;
								//$filesize += dirsize("{$dir}/{$file}/{$subfile}");
							}
							else
							{
								$numfiles++;
								//$filesize += filesize("{$dir}/{$file}/{$subfile}");
							}
						}
					}
					closedir($subdh);
				}
				//$filesize = bytestostr($filesize);
				$numfiles  += $numsubdirs;
				$filesize = "{$numfiles}&nbsp;item".(($numfiles == 1) ? "" : "s");
				$dirlist[] = $file;
			}
			else
			{
				$filesize = filesize("{$dir}/{$file}");
				$totalsize += $filesize;
				$filesize = bytestostr($filesize);
				$filelist[] = $file;
			}
			$sizelist[$file] = $filesize;
			$permlist[$file] = fileperms("{$dir}/{$file}");
			$datelist[$file] = filemtime("{$dir}/{$file}");
		}
	}
	$totalsize = bytestostr($totalsize);
}

$content = "";

$docselect = "
              <select name=\"type\">";
foreach($doctypes as $type => $doctype)
{
	$docselect .= "
                <option value=\"{$type}\">{$doctype[0]}</option>";
}
$docselect .= "
              </select>";


if($lastaction)
{
	$statusmsg = "";
	if($lasterror = gfm_session("lasterror"))
	{
		$statusmsg = "<strong class=\"error\">Error</strong>: {$lasterror}";
		gfm_session("lasterror", "");
	}
	else
	{
		switch($lastaction)
		{
			case "edit":
				$statusmsg = "File \"".htmlspecialchars($lastfile)."\" has been saved.";
			break;
			case "new":
				$statusmsg = "File \"".htmlspecialchars($lastfile)."\" has been created.";
			break;
			case "newdir":
				$statusmsg = "Directory \"".htmlspecialchars($lastfile)."\" has been created.";
			break;
			case "delete":
				$statusmsg = "Item \"".htmlspecialchars($lastfile)."\" has been deleted.";
			break;
			case "rename":
				$statusmsg = "Item \"".htmlspecialchars($lastfile)."\" has been renamed.";
			break;
			case "massrename":
				$statusmsg = "Files &amp; folders of \"".htmlspecialchars((!$lastfile?$dir:$lastfile))." Directory\" has been mass replaced.";
			break;
			case "upload":
				$statusmsg = "File \"".htmlspecialchars($lastfile)."\" has been uploaded.";
			break;
			case "chmod":
				$statusmsg = "Item \"".htmlspecialchars($lastfile)."\" has been chmod'd";
			break;
			case "copy":
				$statusmsg = "Item \"".htmlspecialchars($lastfile)."\" has been copied.";
			break;
			case "unzip":
				$statusmsg = "File \"".htmlspecialchars($lastfile)."\" has been unzipped.";
			break;
		}
	}

	if($statusmsg)
	{
		$content .= "
   <table>
     <tr>
       <th class=\"current filename\">Status</th>
     </tr>
     <tr>
    <td class=\"status\">
      {$statusmsg}
    </td>
     </tr>
   </table>";
	}
}
gfm_session("lastaction", "", "lastfile", "");

$content .= "
<ul id=\"navmenu\">
  <li>".gfmlink("Home Directory", "action=default", "", true)."</li>
  <li>".gfmlink("Reload", "action=default")."</li>
  <li>".gfmlink("Logout", "action=logout")."</li>
  <li>".gfmlink("Help", "action=help")."</li>
  <li>".gfmlink("License", "action=gpl")."</li>
</ul>
<div id=\"control-panel\" class=\"container\">
  <table>
    <tr>
      <th class=\"current filename\">Control Panel</th>
    </tr>
    <tr>
      <td id=\"controls\">
        <form action=\"{$self}\" method=\"post\" id=\"control-new\"".(($control != "new") ? " style=\"display: none;\"" : "").">
          <fieldset class=\"noborder\">
            <input type=\"hidden\" name=\"action\" value=\"new\" />
            {$inputdir}
            <label>Filename (Including Extension):<br /> <input type=\"text\" name=\"filename\" /></label><br />
            <label>
              Type:<br /> {$docselect}
            </label><br />
            <input type=\"submit\" value=\"Create File\" />
          </fieldset>
        </form>
        <form action=\"{$self}\" method=\"post\" id=\"control-newdir\"".(($control != "newdir") ? " style=\"display: none;\"" : "").">
          <fieldset class=\"noborder\">
            <input type=\"hidden\" name=\"action\" value=\"newdir\" />
            {$inputdir}
            <label>Directory Name: <br /> <input type=\"text\" name=\"dirname\" /></label><br />
            <input type=\"submit\" value=\"Create Directory\" />
          </fieldset>
        </form>
        <form action=\"{$self}\" method=\"post\" enctype=\"multipart/form-data\" id=\"control-upload\"".(($control != "upload") ? " style=\"display: none;\"" : "").">
          <fieldset class=\"noborder\">
            <input type=\"hidden\" name=\"action\" value=\"upload\" />
            {$inputdir}
            <label>File: <br /> <input type=\"file\" name=\"newfile\" /></label><br />
            <label>Change filename to lowercase: <input type=\"checkbox\" name=\"lowercase\" /></label><br />
            <label>Overwrite existing file (if exists): <input type=\"checkbox\" name=\"overwrite\" /></label><br />
            <input type=\"submit\" value=\"Upload\" />
            <p>
              Maximium upload size: ".bytestostr($max_upload_size)."
            </p>
          </fieldset>
        </form>
       <form action=\"{$self}\" method=\"post\" id=\"control-massrename\"".($control != "massrename" ? " style=\"display: none;\"" : "").">
          <fieldset class=\"noborder\">
            <input type=\"hidden\" name=\"action\" value=\"massrename\" />
            {$inputdir}
            <input type=\"hidden\" name=\"file\" value=\"\" />
            <label><span>Rename files &amp; folders in mass.<br />From</span>: <input type=\"text\" name=\"newvalue\" value=\"".htmlspecialchars($dir)."\" /></label><br />
            <label><span>To</span>:
            <input type=\"text\" name=\"replace\" value=\"".htmlspecialchars($dir)."\" /></label><br />
            <label>Change with <a target=\"_blank\" title=\"Dev.note: ”From” have already ”~” delimiters in preg_replace function ;)\" href=\"https://secure.php.net/manual/en/function.preg_replace.php\">regex</a>: <input type=\"checkbox\" name=\"preg_replace\" /></label><br />
            <label>Change to lowercase: <input type=\"checkbox\" name=\"strtolower\" /></label><br />
            <label>Change to UPPERCASE: <input type=\"checkbox\" name=\"strtoupper\" /></label><br />
            <label>Folder recursive: <input type=\"checkbox\" name=\"recursive\" /></label><br />
            <input type=\"submit\" value=\"Replace\" />
          </fieldset>
        </form>
        <ul class=\"plainlist\" id=\"control-list\">
          <li>".gfmlink("Create New File", "control=new", "onclick=\"return showControl('new');\"")."</li>
          <li>".gfmlink("Create New Directory", "control=newdir", "onclick=\"return showControl('newdir');\"")."</li>
          <li>".gfmlink("Upload File", "control=upload", "onclick=\"return showControl('upload');\"")."</li>
          <li>".gfmlink("Mass Rename", "control=massrename", "onclick=\"return showControl('massrename');\"")."</li>
          <li>".gfmlink("Download All Files as ZIP", "action=dlzip&amp;file=.", "target=\"_blank\" onclick=\"return confirm('You are about to download EVERY file in this directory, as well as all subdirectories, in a single zip file.  Depending on your filesystem, this may or may not be a good idea.  If you are in a directory that has several gigabytes of data, this could easily crash Galaxy File Manager.  Do you wish to continue?');\"")."</li>
          <li>".gfmlink("PHP Info", "action=phpinfo")."</li>
        </ul>
      </td>
    </tr>
  </table>
  <table>
    <tr>
      <th class=\"current filename\">Directory Statistics</th>
    </tr>
    <tr>
      <td>
        <ul class=\"plainlist\">
          <li>".count($filelist)."&nbsp;file".((count($filelist)) == 1 ? "" : "s").", totaling&nbsp;{$totalsize}&nbsp;/&nbsp;{$disk_free_space}</li>
          <li>".count($dirlist)."&nbsp;subdirectories</li>
        </ul>
      </td>
    </tr>
  </table>
</div>
<div id=\"file-panel\" class=\"container\">";
natsort($dirlist);
natsort($filelist);
$content .= "
  <table>
    <tr>
      <th colspan=\"4\" class=\"current filename\">Files</th>
    </tr>";
foreach(array_reverse($dirlist) as $file)
{
	array_unshift($filelist, $file);
	unset($file);
}
unset($dirlist);

$i = 0;
if($subdir)
{
	$td = "td class=\"filecell".(($i % 2) + 1)."\"";
	$content .= "
    <tr id=\"parent-directory\">
      <{$td} colspan=\"4\">◄ ".gfmlink("Parent Directory", "action=default&amp;dir=".rawurlencode(resolvepath("{$subdir}/..")), "class=\"filename dir\"")."</td>
    </tr>";
	$i++;
}
if(!count($filelist))
{
	$td = "td class=\"filecell".(($i % 2) + 1)."\"";
	$content .= "
    <tr>
      <{$td} colspan=\"4\">Directory is empty.</td>
    </tr>";
	$i++;
}
foreach($filelist as $file)
{
	$htmlfile = htmlspecialchars(str_replace(" ", "-", $file))."-{$i}";
	$urlfile = rawurlencode($file);
	if(is_readable("{$dir}/{$file}"))
	{
		$options = "";
		$optiona = array("", "", "", "");
		switch($option)
		{
			case "rename":
				$optiona = array("Rename to", htmlspecialchars($file), "Rename", "");
			break;
			case "massrename"://NEW array parms noscript version & in js showOption :::: ? value = ''?
				$optiona = array("Rename files &amp; folders in mass.<br />From", htmlspecialchars($file), "Replace", "To");
			break;
			case "chmod":
				$optiona = array("Mode", substr(sprintf('%o', $permlist[$file]), -4), "Chmod", "");
			break;
			case "copy":
				$optiona = array("Copy to", "copy_of_".htmlspecialchars($file), "Copy", "");
			break;
			case "unzip":
				$optiona = array("Unzip to Directory", htmlspecialchars(substr($file, 0, -4)), "Unzip", "");
			break;
			default:
				$option = "";
			break;
		}
		$options .= "<form action=\"{$self}\" method=\"post\" id=\"{$htmlfile}-option\"".($option ? "" : " style=\"display: none;\"").">
          <fieldset class=\"noborder\">
            {$inputdir}
            <input type=\"hidden\" name=\"action\" id=\"{$htmlfile}-option-action\" value=\"{$option}\" />
            <input type=\"hidden\" name=\"file\" value=\"".htmlspecialchars($file)."\" />
            <label><span id=\"{$htmlfile}-option-text\">{$optiona[0]}</span>: <input type=\"text\" name=\"newvalue\" id=\"{$htmlfile}-option-newvalue\" value=\"{$optiona[1]}\" /></label><br />
            <div id=\"{$htmlfile}-option-massrename\"".($option!="massrename"?" style=\"display: none;\"":"").">
              <label><span id=\"{$htmlfile}-option-text-from\">{$optiona[3]}</span>:
              <input type=\"text\" name=\"replace\" id=\"{$htmlfile}-option-replace\" value=\"{$optiona[1]}\" /></label><br />
              <label>Change with <a target=\"_blank\" title=\"Dev.note: ”From” have already ”~” delimiters in preg_replace function ;)\" href=\"https://secure.php.net/manual/en/function.preg_replace.php\">regex</a>: <input type=\"checkbox\" name=\"preg_replace\" /></label><br />
              <label>Change to lowercase: <input type=\"checkbox\" name=\"strtolower\" /></label><br />
              <label>Change to UPPERCASE: <input type=\"checkbox\" name=\"strtoupper\" /></label><br />
              <label>Folder recursive: <input type=\"checkbox\" name=\"recursive\" /></label><br />
            </div>
            <input type=\"submit\" id=\"{$htmlfile}-option-submit\" value=\"{$optiona[2]}\" />
          </fieldset>
        </form>";
		$options .= "
        <p class=\"nospace\">
          Perform action on \"".htmlspecialchars($file)."\":
        </p>
        <ul id=\"{$htmlfile}-option-list\" class=\"plainlist\">
          <li>".gfmlink("Delete", "action=delete&amp;file={$urlfile}", "onclick=\"return confirm('Are you sure you wish to delete &quot;".htmlspecialchars($file)."&quot;?  This action cannot be undone!');\" title=\"Delete &quot;".htmlspecialchars($file)."&quot;\"")."</li>
          <li>".gfmlink("Rename", "action=default&amp;option=rename&amp;file={$urlfile}", "onclick=\"return showOption('{$htmlfile}', '".htmlspecialchars($file)."', 'rename');\"")."</li>
          <li>".gfmlink("Copy", "action=default&amp;option=copy&amp;file={$urlfile}", "onclick=\"return showOption('{$htmlfile}', '".htmlspecialchars($file)."', 'copy');\"")."</li>
          <li>".gfmlink("Chmod", "action=default&amp;option=chmod&amp;file={$urlfile}", "onclick=\"return showOption('{$htmlfile}', '".htmlspecialchars($file)."', 'chmod', '".substr(sprintf('%o', $permlist[$file]), -4)."');\"")."</li>
          <li>".gfmlink("Download as ZIP", "action=dlzip&amp;file={$urlfile}", 'target="_blank"')."</li>";
		$filelink = gfmlink("&#10133;", "action=default&amp;file={$urlfile}", "onclick=\"toggleFileMenu(this, 1); return false;\" class=\"bullet\"")." ";//&bull;
		if(is_dir("{$dir}/{$file}"))
		{
			$filelink .= gfmlink($file, "action=default&amp;dir=".rawurlencode(($subdir) ? "{$subdir}/" : "")."{$urlfile}", "title=\"List the contents of &quot;".htmlspecialchars($file)."&quot;\" class=\"filename dir\"");
			$options .= "
          <li>".gfmlink("Mass Rename", "action=default&amp;option=massrename&amp;file={$urlfile}", "onclick=\"return showOption('{$htmlfile}', '".htmlspecialchars($file)."', 'massrename');\"")."</li>".PHP_EOL;
		}
		else
		{
			$filelink .= "<a href=\"".htmlspecialchars(str_replace(" ", "%20", pathtourl("{$dir}/{$file}")))."\" title=\"View &quot;".htmlspecialchars($file)."&quot; in your web browser\" class=\"filename\">".htmlspecialchars($file)."</a>";
			$options .= ((substr($file, -4) == ".zip") ? "
          <li>".gfmlink("Unzip", "action=default&amp;option=unzip&amp;file={$urlfile}", "onclick=\"return showOption('{$htmlfile}', '".htmlspecialchars($file)."', 'unzip');\"")."</li>" : "
          <li>".gfmlink("View Source", "action=source&amp;file={$urlfile}", "title=\"View the source of &quot;".htmlspecialchars($file)."&quot;\"")."</li>
          <li>".gfmlink("Edit", "action=edit&amp;file={$urlfile}", "title=\"Edit &quot;".htmlspecialchars($file)."&quot;\"")."</li>");
		}
		$options .= "
        </ul>";
		$td = "td class=\"filecell".(($i % 2) + 1)."\"";
		$content .= "
    <tr id=\"{$htmlfile}\">
      <".substr($td, 0, -1)." nrb\">{$filelink}</td>
      <".substr($td, 0, -1)." nrb\"><span class=\"size small\">{$sizelist[$file]}</span></td>
      <".substr($td, 0, -1)." nrb\">".substr(sprintf('%o', $permlist[$file]), -4)."</td>
      <{$td}>".date(get_config_var("DATE_FORMAT"), $datelist[$file])."</td>
    </tr>
    <tr".(($file != @$_GET['file']) ? " style=\"display: none;\"" : "").">
      <td id=\"{$htmlfile}-options\" colspan=\"4\">
        {$options}
      </td>
    </tr>";
		$i++;
	}
}

$content .= "
  </table>
</div>";

echo $content;
