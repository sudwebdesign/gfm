# Galaxy_File_Manager core

## A #StandAlone libre web-based file manager written in PHP and adapted for PluXml ;-)

It's very compact and useful for a variety of tasks. 

### NOTE: A "standalone" version are named ".StandAlone" in this folder is for use it out of PluXml. 
#### how to: 
Open to edit gfm.StandAlone.php file with notepad++ like, Remove exit; line, change example password (and administrator login) by yours, save as gfm.sa.php (for example), push it on your server in your ROOT public html folder & go on your domain add /gfm.ad.php to access it ;)
##### Tips:
If you need access on more folder (like ftp) modifiy "DIR" => "../../" After "//Configuration -> General" line
###### Modify:
```"DIR" => ".",// Default PluXml directory. (#standalone default ".")   No access ```
###### To:
```"DIR" => "../../",// Default PluXml directory. (#standalone default ".")   No access ```
