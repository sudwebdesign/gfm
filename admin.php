<?php if(!defined('PLX_ROOT')) exit;
function gfmHashPlxPass($password,$salt){
 return sha1($salt.md5($password));
}
//include (PLX_PLUGINS.'/gfm/Galaxy_File_Manager/gfm.1.0.1.php');
if ($plxAdmin->aUsers[$_SESSION['user']]['profil'] <= PROFIL_EDITOR){ //si enregisré enclenche le js (CNX AUTO)
?>
<noscript><style type="text/css">.myhide, #gfmDiv{display:none}</style><h4>Javascript is Needed!</h4></noscript>
<script style="display:none;" type="text/javascript">
<!--
//https://www.willmaster.com/library/tutorials/auto-resize-iframe-when-content-size-changes.php
function AdjustIframeHeightOnLoad(f) { document.getElementById(f).style.height = document.getElementById(f).contentWindow.document.body.scrollHeight + "px"; }
function AdjustIframeHeight(f,i) { document.getElementById(f).style.height = parseInt(i) + "px"; }
function resizeFrame(f) {
	AdjustIframeHeightOnLoad(f.id);
}
function linkFrame(u) {//open in new window
 //"linkFrame u : http://pluxml-master.dev/plugins/gfm/Galaxy_File_Manager/gfm.1.0.1.php?action=edit&file=test_filename.php&dir=gfm_test_zip"
	window.document.getElementById("linkFrame").href = u;//console.log('linkFrame u : ' + u); //lost href query after edit file
	onElementHeightChange(window.frames["gfmFrame"].document, function(s){
		AdjustIframeHeight("gfmFrame",s);
	});
}
function onElementHeightChange(elm, callback){// thx vsync: good start : stackoverflow.com/a/14901150
	var lastHeight = elm.body.clientHeight, newHeight;
	(function run(){
		var elm = window.frames["gfmFrame"].document;
		var rsz = true;
		var ea = elm.getElementById("formedit");
		if(ea){//console.log(ea.children[0]);
			fea = ea.children[0].children[0].nextSibling;
			if(fea.id == "frame_filedata" && fea.style.display == "block")
				rsz = false;//prevent editarea bad frame resize on fullwidth, because textarea height unexist (display:none by editarea)
		}
		if(elm.body && rsz)//don't 4 editarea in fullwidth mode
			newHeight = elm.body.clientHeight;
		if(lastHeight != newHeight)
			callback(newHeight);
		lastHeight = newHeight;
		if(elm.body){
			if(elm.body.onElementHeightChangeTimer)
				clearTimeout(elm.body.onElementHeightChangeTimer);
			elm.body.onElementHeightChangeTimer = setTimeout(run, 333);
		}
	})();
}
function login() {
	if (window.frames["gfmFrame"].document.getElementById("login")){//login utilisateur
		var d = window.frames["gfmFrame"].document;
		var f = d.getElementById("login");
		var u = d.getElementById("username");
		var p = d.getElementById("password");
		f.setAttribute("style","display:none;");
//disable-browser-save-password-functionality
		f.setAttribute("autocomplete","off");
		u.type = "hidden";
		p.type = "hidden";
		u.value = "<?php echo $plxAdmin->aUsers[$_SESSION['user']]['login'] ?>";
		p.value = "<?php echo $plxAdmin->aUsers[$_SESSION['user']]['password'] ?>";//hash
		f.submit();//cnx si form login
	}
}
-->
</script>
<?php
}
?>
<div id="gfmDiv" style="width:100%;height:calc(100% - 0px - 12rem);padding-top:23px;margin-top:-20px;" class="div_element_deselect"><br />
	<iframe src="<?php echo PLX_PLUGINS ?>gfm/Galaxy_File_Manager/gfm.php" id="gfmFrame" name="gfmFrame" onload="linkFrame(this.contentWindow.location);login();setTimeout(resizeFrame(this), 333);" width="100%" style="min-height:calc(207% - 0px);width:100%;border:0px;margin-top:-20px;border:none; overflow:hidden;" scrolling="no"></iframe>
</div>
<div class="in-action-bar myhide" style="z-index:1;">
	<a id="linkFrame" href="<?php echo PLX_PLUGINS ?>gfm/Galaxy_File_Manager/gfm.php" target="_blank" title="Open Galaxy File Manager frame in new tab or window">
	<img src="<?php echo PLX_PLUGINS ?>gfm/icon.png"><img src="<?php echo PLX_PLUGINS ?>gfm/open_in_new_window.png" /></a>
</div>
