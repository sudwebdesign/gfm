# Galaxy File Manager

## A Simple #StandAlone libre web-based file manager written in PHP and adapted for PluXml ;).

It's very compact and useful for a variety of tasks.

#### NOTE: A "standalone" version are included in "Galaxy_File_Manager" folder of plugin to use it out of PluXml.
##### How to:
Open to edit gfm.StandAlone.php file with notepad++ like, Remove exit; line, change example password (and administrator login) by yours, save it & use it ;)
###### More info in Galaxy_File_Manager/README.md and Galaxy_File_Manager/VERSION
