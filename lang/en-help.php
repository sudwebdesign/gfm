<?php if(!defined('PLX_ROOT')) exit; ?>
<style>code{background-color: #f1f1f1}</style>
<p class="in-action-bar" style="z-index:1;"><br />
A file manager adapted for PluXml with a variety of tools.
</p>
<h2>How it work:</h2>
<p>When is Activated, this add <i>Galaxy File Manager</i> on PluXml admin menu,<br />
click on it to open GFM with root PluXml folder.</p>
<h2>NOTE:</h2>
<p class="alert green warning">
    A <i>standalone version</i> are included in "Galaxy File Manager" folder of plugin.<br />
    Ready to use it out of PluXml (Pratically*).
</p>
<h2>*how to use out of PluXml:</h2>
<p class="alert orange warning">
   - Open to edit gfm stand alone php file with notepad++ like<br />
   - Remove <i>exit;</i> on line 2, or comment with add # before "exit;" word,<br />
   - Change "example" password and "administrator" login by yours,<br />
   - save it & use it where you want ;)<br />
</p>
