<?php if(!defined('PLX_ROOT')) exit; ?>
<style>code{background-color: #f1f1f1}</style>
<p class="in-action-bar" style="z-index:1;"><br />
Un gestionnaire de fichier adapté pour PluXml avec de nombreux outils.
</p>
<h2>Comment &nbsp;:</h2>
<p>When is Activated, this add <i>Galaxy File Manager</i> on PluXml admin menu,<br />
click on it to open GFM with root PluXml folder.</p>
<h2>NOTE&nbsp;:</h2>
<p class="alert green warning">
    Une version <i>standalone</i> est incluse dans le dossier "Galaxy File Manager" du plugin.<br />
    Il est (presque*) prêt à l'emploi hors de PluXml.
</p>
<h2>*Comment faire pour l'utiliser hors de PluXml&nbsp;:</h2>
<p class="alert orange warning">
   - Ouvrir le fichier gfm stand alone php avec notepad++ (par exemple)<br />
   - Effacer le mot <i>exit;</i> ligne 2, ou commenter en le préfixant d'un add # (diese),<br />
   - Modifier le mot de passe <b>example</b> et <i>administrator</i> avec vos identifiant,<br />
   - Enregistrer let & utiliser lo où bon vous semble where you want ;)<br />
</p>
