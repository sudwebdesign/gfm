<?php if(!defined('PLX_ROOT')) exit;#Plugin gfm (Galaxy File Manager) update of http://www.gamingg.net/ gfm v1.0 * @version 1.0.1 * @date 11/10/2018 * @author sudwebdesign : Thomas Ingles
class gfm extends plxPlugin{
 public function __construct($default_lang){#Constructeur de la classe * @param default_lang par défaut utilisée par PluXml * @retour aucun
  parent::__construct($default_lang);#Appel du constructeur de la classe plxPlugin (obligatoire)
  $this->setAdminProfil(PROFIL_ADMIN, PROFIL_MANAGER , PROFIL_MODERATOR , PROFIL_EDITOR );#Option pour autorisé le type de profil en dynamique : (integer)$_SESSION['profil']. OU Alors PROFIL_ADMIN , PROFIL_MANAGER , PROFIL_MODERATOR , PROFIL_EDITOR , PROFIL_WRITER
  if(defined('PLX_ADMIN')){#Déclaration des hooks pour la zone d'administration
   $this->addHook('AdminAuthEndBody', 'onDeactivate');
   if(strstr($_SERVER['REQUEST_URI'],'plugin.php?p=gfm') && isset($_SESSION['user']))
    $this->addHook('AdminPrepend', 'AdminPrepend');
  }
 }
 public function AdminPrepend(){#créer les variables de session pour transmettre le login et pass a GFM
  echo "<?php
   if(\$plxAdmin->aUsers[\$_SESSION['user']]['profil'] <= PROFIL_EDITOR){
    \$_SESSION['gfmuser'] = \$plxAdmin->aUsers[\$_SESSION['user']]['login'];
    \$_SESSION['gfmpass'] = \$plxAdmin->aUsers[\$_SESSION['user']]['password'];
    \$_SESSION['medias'] = \$plxAdmin->aConf['medias'];//\$plxAdmin->aConf['userfolders']
   } ?>".PHP_EOL;
 }
 public function onDeactivate() {#Méthode exécutée à la désactivation du plugin et a la (dé)connexion de PluXml [hook AdminAuthEndBody]
  if(isset($_SESSION['gfm'])){
   unset($_SESSION['gfm'],$_SESSION['gfmuser'],$_SESSION['gfmpass']);
  }
 }
}
